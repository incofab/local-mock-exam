<?php

if(isset($dontRoute)) return;
    
global $routesArr;
$routesArr = require_once APP_DIR . '../Bootstrap/routes/routeNames.php';

$addr = rtrim(ADDR, '/');

/**
 * @var \FastRoute\Dispatcher
 */
$dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r)use ($routesArr, $addr) {

    $r->addRoute(['GET'], $addr.$routesArr['home'], ['\App\Controllers\Home\Home', 'index']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_login'], [ '\App\Controllers\Home\Login', 'adminLogin']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_dashboard'], [ '\App\Controllers\Admin\Admin', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_all'], [ '\App\Controllers\Admin\Admin', 'all']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_add'], [ '\App\Controllers\Admin\Admin', 'addNew']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_edit'], [ '\App\Controllers\Admin\Admin', 'edit']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_delete'] . '/{table_id}', [ '\App\Controllers\Admin\Admin', 'delete']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_changepassword'], [ '\App\Controllers\Admin\Admin', 'changePassword']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_logout'], [ '\App\Controllers\Admin\Admin', 'logout']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_event_index'], [ '\App\Controllers\Admin\EventController', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_event_download'].'[/{eventId}]', [ '\App\Controllers\Admin\EventController', 'downloadEventContent']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_event_upload'].'[/{eventId}]', [ '\App\Controllers\Admin\EventController', 'uploadEventResult']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_sms'] . '[/{phoneNo}]', [ '\App\Controllers\Admin\Admin', 'sendSMS']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_notification'] . '/{username}', [ '\App\Controllers\Admin\Admin', 'sendNotification']);
    
    
    require_once APP_DIR . 'rough.php';
    
});

    
    // Fetch method and URI from somewhere
    $httpMethod = $_SERVER['REQUEST_METHOD'];
    
    $uri = $_SERVER['REQUEST_URI'];
    
    // Strip query string (?foo=bar) and decode URI
    if (false !== $pos = strpos($uri, '?')) 
    {
        $uri = substr($uri, 0, $pos);
    }
    
    $uri = rawurldecode($uri);
    
    $routeInfo = $dispatcher->dispatch($httpMethod, $uri);
    
    switch ($routeInfo[0]) 
    {
        case FastRoute\Dispatcher::NOT_FOUND:
//             require APP_DIR . '../resources/views/home/404.php';
            echo view('home/404');
            break;
            
        case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
            $allowedMethods = $routeInfo[1];
            echo '405 Method Not Allowed';
            break;
            
        case FastRoute\Dispatcher::FOUND:
            $handler = $routeInfo[1];
            $parameters = $routeInfo[2];
            $parameters = array_values($parameters);
//             dlog($uri); 
//             dlog($parameters); 
//             dDie($handler); 
//             call_user_func_array([new $handler[0], $handler[1]], $parameters);
            try { 
                $container = (new \Bootstrap\Container\MyContainer())->getContainerInstance();
                $content = $container->call($handler, $parameters);
                if(is_array($content)) $content = json_encode($content);
                echo $content;
            } catch (Exception $e) {
//                 dlog("Error in url $uri, SERVER['REQUEST_URI'] = {$_SERVER['REQUEST_URI']}");
//                 dlog("Error in file {$e->getFile()}, line = {$e->getLine()}");
//                 dlog($e->getTrace());
                throw $e;
            }
            break;
    }
