<?php

$routesArr = [
    
    'home' => '/',
    
    /* Login Routes */
    'admin_login' => '/admin/login',
    
    /* Admin Routes */
    'admin_dashboard' => '/admin',
    'admin_all' => '/admin/all-admin-users',
    'admin_add' => '/admin/add-new-admin-user',
    'admin_edit' => '/admin/edit-admin-user',
    'admin_delete' => '/admin/delete-admin-user',
    'admin_changepassword' => '/admin/changepassword',
    'admin_logout' => '/admin/logout',
    
    'admin_event_index' => '/admin/events',
    'admin_event_download' => '/admin/event/download-content',
    'admin_event_upload' => '/admin/event/upload-result',
        
    'admin_send_sms' => '/admin/send-sms',
    'admin_send_notification' => '/admin/send-notification',
    
    
];


return $routesArr;
