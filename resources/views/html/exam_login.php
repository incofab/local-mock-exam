<?php
$title = "Login Student for Exam | " . SITE_TITLE;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?php echo assets('favicon.ico')?>" >
    <title><?php echo $title ?></title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo assets('lib/bootstrap4/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo assets('lib/font-awesome-4.6.3/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo assets('lib/jquery.min.js') ?>"></script>
    <link href="<?php echo assets('css/vali.css') ?>" rel="stylesheet">
</head>
<style>
.pointer{cursor: pointer;}
</style>

<body class="app sidebar-mini rtl">


    <section class="material-half-bg">
    	<div class="cover"></div>
    </section>
    <section class="login-content">
    	<div class="logo">
    		<h1><?php echo SITE_TITLE ?></h1>
    	</div>
    	<div class="login-box">
    		<form class="login-form" action="" method="get" autocomplete="off">
    			<h3 class="login-head">
                    <small>
                    	<i class="fa fa-lg fa-fw fa-graduation-cap small"></i> &nbsp;&nbsp;
                        Exam Page <?php echo date('Y') ?>
                    </small>
    			</h3>
            	<?php if(!empty($error)): ?>
            	<div class="alert alert-danger">
           			<div><i class="fa fa-star" style="color: #cc4141;"></i> <?php echo $error ?></div>
            	</div>
            	<?php endif; ?>
            	<br /><br />
    			<div class="form-group">
    				<label class="control-label">Exam Number</label>
    				<input class="form-control" type="text" placeholder="Exam No"
    					name="exam_no" autocomplete="off">
    			</div>
    			<br />
    			<div class="form-group btn-container">
    				<button class="btn btn-primary btn-block">
    					<i class="fa fa-sign-in fa-lg fa-fw"></i>Start Exam
    				</button>
    			</div>
    		</form>
    	</div>
    </section>


	<script type="text/javascript" src="<?php echo assets('lib/bootstrap4/js/bootstrap.bundle.min.js') ?>"></script>
	<script src="<?php echo assets('js/vali.js')?>"></script>
	<script src="<?php echo assets('lib/pace/pace.min.js')?>"></script>
	
</body>
</html>