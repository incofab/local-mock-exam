<?php
$title = "Admin - All Administrative Users | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin'
?>
@extends('admin.layout')

@section('dashboard_content')

	<div >
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active"><i class="fa fa-users"></i> Administrators</li>
		</ol>
		<h4 class="">Admin Users</h4>
	</div>
    <div style="margin-bottom: 10px;">
    	<a href="{{getAddr('admin_add')}}" class="btn btn-success pull-right" >
    		<i class="fa fa-plus"></i> New
    	</a>
    	<div class="clearfix"></div>
    </div>
	<div>
		<table class="table table-responsive table-striped">
			<tr>
				<th>Username</th>
				<th>Email</th>
				<th>Access Level</th>
				<th></th>
			</tr>
			@foreach($allRecords as $record)
				<tr>
					<td>{{$record[USERNAME]}}</td>
					<td>{{$record[EMAIL]}}</td>
					<td>{{$record->getLevel()}}</td>
					<td>
						<a href="{{getAddr('admin_delete', $record[TABLE_ID])}}" 
							onclick="return confirm('Are you sure?')"
							class="btn btn-sm btn-danger"> <i class="fa fa-trash"></i> </a>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	

@stop
