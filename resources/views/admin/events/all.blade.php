<?php
use App\Controllers\Helpers\EventContentHandler;

$title = "Exam Center - All Events | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';
$confirmMsg = 'Are you sure?';
?>
@extends('admin.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Events
		</h1>
		<p>List of all Events</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Exams</li>
	</ul>
</div>
	
<div class="tile">
	@include('common.form_message')
    <div class="tile-body">
    	<table class="table table-hover table-bordered" id="data-table" >
    		<thead>
    			<tr>
    				<th>S/No</th>
    				<th>Title</th>
    				<th>Description</th>
    				<th>Duration</th>
    				<th>Options</th>
    			</tr>
    		</thead>
    		<?php $i = 0; ?>
			@foreach($allRecords as $record)
			<?php 
			 $eventSubjects = $record['event_subjects'];
			 $subjects = '';
			 foreach ($eventSubjects as $subject) {
			     $subjects .= $subject['course_id'] . ',';
			 }
			 $subjects = rtrim($subjects, ',');
			 $dur = \App\Core\Settings::splitTime($record[DURATION]);
			 $i++;
			?>
				<tr title="Subjects: [{{$subjects}}]" >
					<td>{{$i}}</td>
					<td>{{$record[TITLE]}}</td>
					<td>{{$record[DESCRIPTION]}}</td>
					<td><?= "{$dur['hours']}hrs, {$dur['minutes']}mins, {$dur['seconds']}secs" ?></td>
					<td><?php ?>
						@if(EventContentHandler::isDownloaded($record['id']))
						<a href="{{getAddr('admin_event_download', [$record['id']])}}" class=""
						onclick="return confirm('Redownload this event. \nThis will delete already existing records and this event. \nAre you sure?')">Download Again</a>

						<a href="{{getAddr('admin_event_upload', [$record['id']])}}" class="btn btn-success"
						onclick="return confirm('Upload completed exams?')"
						><i class="fa fa-upload"></i> Upload Exams</a>
						@else
						<a href="{{getAddr('admin_event_download', [$record['id']])}}" class="btn btn-primary"
						onclick="return confirm('Are you sure?')"
						><i class="fa fa-download"></i> Download Content</a>
						@endif
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="tile-footer">
		@include('common.paginate')
	</div>
</div>

<!-- Data table plugin-->
<script type="text/javascript" src="{{assets('lib/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('#data-table').DataTable({
    "iDisplayLength": 50
});
$(function () {
//   $('[data-toggle="popover"]').popover();
  var popOverSettings = {
//	 	    placement: 'bottom',
//	 	    container: 'body',
//	 	    html: true,
		    selector: '[data-toggle="popover"]', //Sepcify the selector here
//	 	    content: function () {
//	 	        return $('#popover-content').html();
//	 	    }
	}
	
	$('#data-table').popover(popOverSettings);
});
function confirmAction() {
	return confirm('{{$confirmMsg}}');
}
</script>

@stop
