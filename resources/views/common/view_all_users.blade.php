
<div >
	<h3 class="">Users</h3>
	<!-- /.row -->
	<div class="row">
		<div class="panel-body">
			<div class="">
				<table width="100%" class="table table-striped table-responsive" id="dataTables-example">
					<thead>
						<tr>
							<th>S/No</th>
							<th>Username</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Balance</th>
							<th>Operation</th>
						</tr>
					</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($allRecords as $user)
						<tr class='odd gradeX lim'>
							<td>{{ $i }}</td>
							<td>{{ $user[USERNAME] }}</td>
							<td>{{ $user[PHONE_NO] }}</td>
							<td>{{ $user[EMAIL] }}</td>
							<td>{{CURRENCY_SIGN}}{{ $user->getBalance() }}</td>
							
							<td>
								<a href="<?= getAddr('admin_view_user_profile', $user[TABLE_ID]) ?>" style='margin-left:5px'><i class='fa fa-user fa-fw'></i> Profile</a>
								<br />
								<a href="<?= getAddr('admin_delete_user', $user[TABLE_ID]) ?>" 
									style='margin-left:5px' onclick="return confirm('Are you sure?')" 
									><i class='fa fa-trash fa-fw'></i> Delete
								</a>
							</td>
							
						</tr>
					<?php $i++; ?>
					@endforeach
				</tbody>
				</table>
			</div>
		<!-- /.table-responsive -->
		</div>
	</div>

	 


<!-- /.row -->
</div>
<!-- /#page-wrapper -->
<?php 

?>