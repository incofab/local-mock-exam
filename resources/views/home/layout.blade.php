<?php 
    $isHome = true;
    
    $title = isset($title) ? $title : 'Quickly convert your airtime to cash | ' . SITE_TITLE;
?>
<!DOCTYPE html>

<!--[if lte IE 8]><html class="ie ie8"><![endif]-->
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<!--[if gt IE 9]><!--> <html><!--<![endif]-->

<head>
@if(!DEV) @include('common.google-analytics') @endif
<meta charset="utf-8">
<title>{{$title}}</title>
<meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="U+q2YuAOQjhhxdQwI6qYnx9L6MgEfAFzMoN3WqBukOz0AgEEGrTY1bgdVopVUVHlSpjwWdra9HUGr0X8+D03pg==" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="Quickly and easily convert your airtime to money. Buy and sell products with airtime" />
<meta name="keywords" content="Convert Airtime to money, cash, bank notes, physical cash, quick money, " />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<meta name="msvalidate.01" content="80AFA74FB0D52E01BACFDB81E0C3F2CC" />

<link href="{{assets('css/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{assets('css/lib/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/animate.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/ionicons.min.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/magnific-popup.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/slick.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/themify-icons.css')}}" rel="stylesheet">
<link href="{{assets('css/style.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/responsive.css')}}" rel="stylesheet">

<script type="text/javascript" src="{{ assets('js/lib/jquery.min.js')}}"></script>

@include('common.favicon')

<meta name="robots" content="Index,follow" />
</head>
<body>
<style>
body{
    background-image: url("{{assets('img/images/welcome-bg.png')}}");
    background-position: center;
    background-size: cover;
}
section{overflow: hidden !important;}
b{font-weight: bold;}
.header-pad{height: 110px;}
.no-box-shadow{ box-shadow: none !important; border: none !important;}
.h-auto{ height: auto !important; }
.color-primary{ color: #fb397d !important; }
.color-bg{ color: #5b32b4 !important; }
.footer-margin{ margin-bottom: 374px !important; }
.pointer{cursor: pointer;}
.slab {
  -webkit-box-shadow: 0px 0px 5px 1px #a19a9a;
  -moz-box-shadow: 0px 0px 5px 1px #a19a9a;
  box-shadow: 0px 0px 5px 1px #a19a9a;
  border: 1px solid #93828e22;
  background-color: #fff;
  color: #333333;
  border-radius: 3px;
}
</style>
    @include('home.header')
    @include('common.message')
    @yield('content')
    @include('home.footer')
    
<script type="text/javascript" src="{{assets('lib/popper.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/plugins.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/slick.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/footer-reveal.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/active.js')}}"></script>

</body>
</html>
