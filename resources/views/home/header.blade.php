<!-- ***** Header Area Start ***** -->
<header class="header_area animated">
    <div class="container-fluid">
        <div class="row align-items-center">
            <!-- Menu Area Start -->
            <div class="col-12 col-lg-10">
                <div class="menu_area">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- Logo -->
                        <a class="navbar-brand" href="{{getAddr('home')}}"> 
                        	<?php /* {{SITE_TITLE}} */ ?>
                        	<img src="{{assets('img/cheetahpay-header-img.png')}}" class="img-fluid pl-1" alt="Cheetahpay Header Logo" />
                        </a>
                        <button class="navbar-toggler mt-2" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Menu Area -->
                        <div class="collapse navbar-collapse" id="ca-navbar">
                            <ul class="navbar-nav ml-auto" id="nav">
                                <li class="nav-item active"><a class="nav-link" href="{{getAddr('home')}}">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                                <li class="nav-item"><a class="nav-link" href="#how-it-works">How it works</a></li>
                                <li class="nav-item"><a class="nav-link" href="#developers">Developers</a></li>
                                <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                            </ul>
                            <div class="sing-up-button d-lg-none">
                                <a href="{{getAddr('register_user')}}">Sign Up Free</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Signup btn -->
            <div class="col-12 col-lg-2">
                <div class="sing-up-button d-none d-lg-block">
                    <a href="{{getAddr('register_user')}}">Sign Up Free</a>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="quick-contact">
	<div class="right-top px-1">
		<i class="fa fa-phone-square pl-1"></i>
		<img src="{{assets('img/whatsapp.png')}}" alt="" class="px-1 mh-100" />
		<a href="https://api.whatsapp.com/send?phone={{WHATSAPP_NO }}">07062453689</a>
	</div>
	<div class="left-bottom px-1">
		<i class="fa fa-phone-square pl-1"></i>
		<img src="{{assets('img/whatsapp.png')}}" alt="" class="px-1 mh-100" />
		<a href="https://api.whatsapp.com/send?phone={{WHATSAPP_NO }}">{{WHATSAPP_NO}}</a>
	</div>
</div>
<!-- ***** Header Area End ***** -->
<style>
.header_area.sticky {
    height: 70px;
}
.header_area .col-12{
    padding: 0 !important;
}
.header_area .navbar{
/*     padding: 0 !important; */
}
.header_area .navbar-brand img{
    height: 66px;
    margin-top: 2px;
}
.header_area .navbar-brand{
    max-width: 70%;
    text-shadow:0px 4px 5px #ff9900;
    color: #fff;
}
#quick-contact .right-top, 
#quick-contact .left-bottom{
    position: fixed;
    z-index: 434;
    font-size: 0.9rem;
}
#quick-contact .right-top a, 
#quick-contact .left-bottom a{
    color: inherit !important;
    font-size: inherit !important;
}
#quick-contact .left-bottom{
    background-color: #f51866; 
    color: #fff;
    bottom: 0;
    left: 0;
}
#quick-contact .right-top{
    background-color: #fff; 
    color: #5a10c3;
    top: 0;
    right: 0;
} 
@media (max-width:480px)  {
    .header_area .navbar-brand{
        max-width: 70%;
        font-size: 1.9em;
    }
    .header_area.sticky .menu_area .navbar-brand {
        font-size: 37px;
    }
    #quick-contact .right-top, 
    #quick-contact .left-bottom{
        font-size: 0.8rem;
    }
}
</style>