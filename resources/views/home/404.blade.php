<?php

?>
@extends('vali_layout')

@section('body')


<section class="material-half-bg">
	<div class="cover"></div>
</section>
<section class="login-content">
	<div class="logo">
		<h1>{{SITE_TITLE}}</h1>
	</div>
	<div class="login-box">
		<form class="login-form" action="" method="post">
			<div class="login-head text-center">
				<div class="h2"><i class="fa fa-exclamation-triangle"></i> Ooooops!!!</div> 
				<div class="h5 mt-5">Page Not Found</div>
			</div>
    		<br />
    		<p class="">Sorry, the page you were trying to view does not exist.</p>
		</form>
	</div>
</section>


@endsection