<?php 
    $title = 'Login | ' . SITE_TITLE;
	$post = isset($post) ? $post : []; 
	$login = true;
?>
@extends('home.layout')

@section('content')

<style>
    #login-cover{
/*         background-image: url("{{assets('img/images/abstract.jpg')}}"); */
/*         background-image: url("{{assets('img/images/welcome-bg.png')}}"); */
    }
</style>

<div id="login-cover" class="pb-4">
	<div class="header-pad"></div>
	<div class="row mx-0">
		<div class="col-sm-6 offset-sm-3 col-md-6 offset-md-3" id="login" >
			<div class="slab p-3 mb-2 mt-4">
    			<h3 class="text-center">Login</h3>
    			<form method="POST" action="" name="login" autocomplete="on"> 
    				<br />
    				<div class="form-group" >
						<div class="field-group">
							<i class="fa fa-user"></i>
							<input type="text" name="<?= USERNAME ?>" class="form-control" placeholder="Username or Phone No" 
								required="required" value="{{$username}}" />
						</div>
    				</div>
    				<div class="form-group" >
						<div class="field-group">
    						<i class="fa fa-key"></i>
        					<input type="Password" name="<?= PASSWORD?>" placeholder="Enter password" 
        						required="required" class="form-control" />
        				</div>
    				</div>
    				<div class="form-group py-3" >
        				<input type="hidden" name="login" value="true" />
        				<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
    					<a href="{{getAddr('forgot_password')}}" class="color-bg pull-left" >Forgot password?</a> 
    					<button type="submit" name="login" class="btn submit-btn pull-right p-1 h-auto" value="Login">Login</button>
    					<div class="clearfix"></div>
    				</div>
    			</form>
    			<p class="text-center my-0" >Don't have an account yet? <strong> 
    				<a href="<?= getAddr('register_user')?>" class="color-primary">Create account</a> </strong> 
    			</p>
			</div>
			<br />
			<br />
		</div>
	</div>
</div>
<style>
#login-cover form .field-group{
    position: relative;
}
#login-cover form .field-group i{
    border-right: 1px solid #ced4da;
    display: inline-block;
    width: 38px;
    text-align: center;
    position: absolute;
    left: 1px;
    top: 1px;
    bottom: 1px;
    background-color: #f9fafb;
    font-size: 1.3rem;
    padding-top: 8px;
}
#login-cover form .field-group .form-control{
    padding-left: 40px;
    border-radius: 0;
}
</style>
@stop