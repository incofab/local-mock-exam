<?php
namespace App\Controllers;

class BaseController{

    protected $session;
    protected $container;
    
    protected $page;
    protected $num = 1000;
    
    function __construct(\Bootstrap\Container\MyContainer $c, \Session $session) 
    {
	    $this->session = $session;
	    
	    $this->container = $c->getContainerInstance();
	    
	    $this->page = array_get($_REQUEST, 'page', 1);
	}
	 
	protected function isUserLoggedIn()
	{
	    $data = $this->session->get(USER_SESSION_DATA, null);
	    
	    if(!$data) return null;
	    
	    return $data;
	}
	
	protected function view($view, array $data = [])
	{
// 	    $data['data'] = isset($data['data']) ? $data['data'] : $this->isUserLoggedIn();

	    $data['next'] = isset($data['next']) ? $data['next'] : getAddr('');
		
	    $data['page'] = isset($data['page']) ? $data['page'] : $this->page;

	    $data['numPerPage'] = isset($data['numPerPage']) ? $data['numPerPage'] : $this->num;
	    
	    $data['sessionModel'] = $this->session;
	    
	    return view($view, $data);
	}
	
	protected function logout_($routeTo, $fromAPI = false) 
	{
		\Session::clearSessionData();
	
		\Session::flash('report', 'You are logged out');
		
		if($fromAPI) return [SUCCESSFUL => true, MESSAGE => 'User logged out'];
		
		redirect_(getAddr($routeTo));
	}
	
	protected function checkCSRFToken($token, $redirectOnFailure, $routeName = null) 
	{
	    //Check CSRF token match
	    if (!$this->session->isCsrfValid($token))
	    {
	        if($redirectOnFailure) 
	        {
    	        $this->session->flash('error', 'Token Mismatch');
    	        
	            redirect_(getAddr($routeName));
	        }
	        return false;
	    }
	    return true;
	}
		
	function displayExamResult($examNo, $studentID, $redirectAddr=null)
	{
	    /** @var \App\Core\SubscriptionPlan $subscriptionPlan */
	    $subscriptionPlan = $this->container->get(\App\Core\SubscriptionPlan::class);
	    
	    $examsModel = $this->container->get(\App\Models\Exams::class);
	    
	    $redirectAddr = $redirectAddr ? $redirectAddr : array_get($_REQUEST, 'next', getReferer());
	    
	    /** @var \App\Models\Exams $examData */
	    $examData = $examsModel->where(STUDENT_ID, '=', $studentID)
        	    ->with(['student', 'examSubjects', 'event'])
        	    ->where(EXAM_NO, '=', $examNo)->first();
	    
	    if(!$examData)
	    {
	        $this->session->flash('error', 'Exam not found');
	        
	        redirect_($redirectAddr);
	    }
	    
	    if($examData[STATUS] == STATUS_ACTIVE || $examData[STATUS] == STATUS_PAUSED)
	    {
	        $this->session->flash('error', 'Exam is still active');
	        
	        redirect_($redirectAddr);
	    }
	    
	    /** @var \App\Models\Events $event */
	    $event = $examData['event'];
	    
	    $subscriptionPlan->init($event[CENTER_CODE], $redirectAddr);
	    
	    $isPerEventSub = $subscriptionPlan->isPerEventSubscription($redirectAddr);
	    
	    $isExpired = $subscriptionPlan->isExpired($redirectAddr);
	    
	    if(!$isPerEventSub && $isExpired)
	    {
	        $this->session->flash('error', 'You do not have an subscription');
	        
	        redirect_($redirectAddr);
	    }
	    
	    if($isPerEventSub)
	    {
	        if(empty($event[NUM_OF_ACTIVATIONS]))
	        {
	            $this->session->flash('error', 'This event has not been activated');
	            
	            redirect_($redirectAddr);
	        }
	        
	        if($event[NUM_OF_ACTIVATIONS] < $event->exams()->get()->count())
	        {
	            $this->session->flash('error', 'Some Exams in this event have not been activated');
	            
	            redirect_($redirectAddr);
	        }
	    }
	    
	    $student = $examData['student'];
	    
	    /** @var \App\Models\ExamSubjects $examSubject */
	    $examSubjects = $examData['examSubjects'];
	    
	    $examCenter = $event->examCenter()->first();
	    
	    $result = [];
	    $scorePercent = 0;
	    $subjectsCourseCode = [];
	    
	    foreach ($examSubjects as $examSubject)
	    {
	        $course = $examSubject->course()->first();
	        
	        $result[$examSubject[TABLE_ID]] = [
	            'number_of_questions' => $examSubject[NUM_OF_QUESTIONS],
	            
	            COURSE_TITLE => $course[COURSE_TITLE],
	            
	            'score' => $examSubject[SCORE],
	        ];
	        
	        $subjectsCourseCode[] = $course[COURSE_CODE];
	        
	        $scorePercent += \App\Core\Settings::getPercentage($examSubject[SCORE], $examSubject[NUM_OF_QUESTIONS], 0);
	    }
	    
	    return $this->view('exams/view_single_result', [
	        'result_detail' => $result,
	        'student' => $student,
	        'event' => $event,
	        'examCenter' => $examCenter,
	        'examData' => $examData,
	        'subjectsCourseCode' => $subjectsCourseCode,
	        'totalScore' => $examData[SCORE],
	        'totalNumOfQuestions' => $examData[NUM_OF_QUESTIONS],
	        'total_score_percent' => $scorePercent,
	        'total_num_of_questions_percent' => $examSubjects->count() * 100,
	    ]);
	}
	
	function cehckForUnsupportedBrowser()
	{
	    $obj = new \Detection\MobileDetect();
	    
	    // 	    if(!$obj->isMobile()) return;
	    
	    if($obj->version('Opera Mini') || $obj->version('Opera Mobi') ||
	        $obj->version('UC Browser'))
	    {
	        redirect_(getAddr('home_message', 'unsupported_browser'));
	    }
	}


}