<?php
namespace App\Controllers\Helpers;

class EventContentHandler
{
    const BASE_DIR = APP_DIR."../public/events-export/";
    const EXAM_BASE_DIR = APP_DIR."../public/exams/";
    const UPLOADED_SUFIX = '.uploaded';
    
    public static $baseUrl = (DEV ? 'http://localhost:8000/' : 'https://mock.examscholars.com/');
//     public static $baseUrl = (DEV ? 'http://localhost:8000/' : 'http://exam.inktutors.com/');
    
    private $contentUrl = '';
    
    function __construct() 
    {
        if(!file_exists(self::BASE_DIR))
        {
            mkdir(self::BASE_DIR, 0777, true);
            
            $gitIgnore = '#Ignore all files except the gitignore file'.PHP_EOL.'*'.PHP_EOL.'!.gitignore';

            file_put_contents(self::BASE_DIR.'.gitignore', $gitIgnore);
        }
        
        $this->contentUrl = self::$baseUrl . '';
    }
    
    function listEvents($centerCode) 
    {
        $ret = self::executeCurl(self::$baseUrl.'api/institution/event/index', ['institution_code' => $centerCode]);
        
        return $ret;
    }
            
    function uploadEventExams($eventId) 
    {
        $ret = $this->getEventExams($eventId);
        
        if(!$ret[SUCCESSFUL]) return $ret;

        if(empty($ret['exams'])) return retF('No results to upload');
        
        $exams = $ret['exams'];
        
        $ret = self::executeCurl(self::$baseUrl.'api/institution/event/upload', ['exams' => $exams]);
        
        if($ret[SUCCESSFUL])
        {
            foreach ($exams as $exam)
            {
                $examFile = EventContentHandler::EXAM_BASE_DIR
                ."event_$eventId/exam_{$exam[EXAM_NO]}-student_{$exam[STUDENT_ID]}.edr";
                
                if(!file_exists($examFile)) continue;
                
                rename($examFile, $examFile.self::UPLOADED_SUFIX);
            }
        }
//         dDie($ret);
        return $ret;
    }
    
    function fetchEventContent($post)
    {
        $centerCode = $post['center_code'];
        $eventId = $post['event_id'];
        
        $ret = $this->download($centerCode, $eventId);
        
        if(!$ret) return $ret;
        
        $ret = \App\Core\Helper::unzip($ret['result'], self::BASE_DIR."event-$eventId");
        
        return $ret;
    }
    
    public static function isDownloaded($eventId){
        return file_exists(self::BASE_DIR."event-$eventId");
    }
    
    private function download($centerCode, $eventId)
    {
        $rootName = "event-$eventId";
        
        $file_url = self::$baseUrl.'api/institution/event/download?'.http_build_query(['center_code' => $centerCode, 'event_id' => $eventId]);
        
        $destination_path = self::BASE_DIR.$rootName.'.zip';
        
        // Delete old file if it exists
        if(file_exists($destination_path)) unlink($destination_path);
        
        $fp = fopen($destination_path, "w+");
        
        $ch = curl_init($file_url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        
        $st_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
        fclose($fp);
        
        if($st_code == 200) return retS('File downloaded successfully!', $destination_path);
        
        else return retF('Error downloading file!');
    }
    
    private function getEventExams($eventId){
        
        $eventDir = EventContentHandler::EXAM_BASE_DIR."event_$eventId";
        
        if(!file_exists($eventDir)){
            return retF('Event folder not found');
        }
        
        $dirs = scandir($eventDir);
        
        $examsArr = [];
        
        foreach ($dirs as $dir)
        {
            if(!starts_with($dir, 'exam') || ends_with($dir, self::UPLOADED_SUFIX)) continue;
            
            // Getting here means the exam exists
            $examFilename = "$eventDir/$dir";
            
            if(!file_exists($examFilename)) continue;
            
            $resultData = json_decode(file_get_contents($examFilename), true);
            
            if(empty($resultData['exam']) || $resultData['exam']['status'] == 'active'){
                //Upload only files that have completed their exams
                continue;
            }
            
            unset($resultData['attempts']);
            unset($resultData['exam']['student']);
            
            $examsArr[] = $resultData['exam'];
        }
        
        return [SUCCESSFUL => true, MESSAGE => '', 'exams' => $examsArr];
    }
    
    static function executeCurl($url, $data)
    {
        $curl = curl_init($url);
        
        $curlOptions = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ];
        
        curl_setopt_array($curl, $curlOptions);
        
        $ret = curl_exec($curl);
        
        $error = curl_error($curl);
        curl_close($curl);
        
        if ($error){
            dlog($error);
            return [SUCCESSFUL => false, MESSAGE => 'Error processing request', 'error' => $error ];
        }
        
        $result = json_decode($ret, true);
//         dlog($ret);
//         dlog($result);
        if (!$result){
            return [SUCCESSFUL => false, MESSAGE => 'Error processing request response', 'error' => $ret ];
        }
        
        return $result;
    }
}



