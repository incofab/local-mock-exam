<?php
namespace App\Controllers\Helpers;

require_once APP_DIR.'lib/Carbon/autoload.php';

use Carbon\Carbon;

class ExamHelper
{
    const TIME_FORMAT = 'Y-m-d H:i:s';
    
    private $examHandler;
    
    function __construct(
        \App\Controllers\Helpers\ExamHandler $examHandler
    ){
        $this->examHandler = $examHandler;
    }
    
    function startExam($examNo)
    {
        $examAndEvent = $this->getExamAndEvent($examNo);
        
        if(!$examAndEvent) return retF('Exam details not found');

        $exam = (array)$examAndEvent['exam'];
        $event = $examAndEvent['event'];
        $student = $exam['student'];
        $examSubjects = $exam['exam_subjects'];
        
        if($event->status != STATUS_ACTIVE) return retF("{$event->title} is not started yet");
        
        if($this->examHandler->isExamUploaded($exam['event_id'], $exam['exam_no'], $exam['student_id'])){
            return retF('Exam already completed');
        }
        
        $allQuestionAttempts = [];
        $contentRet = $this->examHandler->getContent($exam['event_id'], $exam['exam_no'], $exam['student_id'], false);
        
        if($contentRet['success'] && !empty($contentRet['content'])){
            
            $exam = $contentRet['content']['exam'];
            
            $allQuestionAttempts = $contentRet['content']['attempts'];
        }
        
        if(!in_array($exam['status'], [STATUS_ACTIVE, STATUS_PAUSED])) return retF('Exam inactive');
        
        if ($exam[END_TIME] && (Carbon::parse($exam[END_TIME])->getTimestamp() < Carbon::now()->getTimestamp()))
        {
            $this->endExam($exam);
            
            return retF('Time elapsed, Exam ended');
        }
        
        $arr = [];
        
        foreach ($examSubjects as $examSubject)
        {
            $session = $this->getSession($examSubject->course_session_id);
            
            $subject = $session['course'];
            
            $questions = $session['questions'];
            
            $questionsFormated = [];
            
            $attemptedQuestionsFormated = [];
            
            foreach ($questions as $question)
            {
                $qArr = (array)$question;
                
                unset($qArr['answer']);
                unset($qArr['answer_meta']);
                
                $qArr['question_id'] = $question['id'];
                $questionsFormated[] = $qArr;
            }
            
            $questionAttempts = getValue($allQuestionAttempts, $examSubject->id, []);
            /*
            foreach ($questionAttempts as $questionAttempt)
            {
                $attemptedQuestionsFormated[$questionAttempt['question_id']] = [
                    'tab_index' => $questionAttempt['tab_index'],
                    'exam_subject_id' => $questionAttempt['exam_subject_id'],
                    QUESTION_ID => $questionAttempt[QUESTION_ID],
                    ATTEMPT => $questionAttempt[ATTEMPT],
                ];
            }
            
            //Override
            $attemptedQuestionsFormated = $questionAttempts;
            */
            
            $passages = $session['passages'];
            
            $instructions = $session['instructions'];
            
            $subjectDataFormatted = [
                EXAM_SUBJECT_ID => $examSubject->id,
                'session_id' => $session['id'],
                'course_id' => $subject['id'],
                'course_code' => $subject['course_code'],
                'course_title' => $subject['course_title'],
                'year' => $session['session'],
                GENERAL_INSTRUCTIONS => $session['general_instructions'],
                'instructions' => (array)$instructions,
                'passages' => (array)$passages,
                'attempted_questions' => $questionAttempts,
                'questions' => $questionsFormated,
            ];
            
            $arr['all_exam_subject_data'][] = $subjectDataFormatted;
//             dlog($attemptedQuestionsFormated);
        }
        
        if($exam['status'] == STATUS_PAUSED)
        {
            $timeElapsed = Carbon::parse($exam[START_TIME])->diffInSeconds(Carbon::parse($exam[PAUSED_TIME]), false);
            
            $timeRemaining = $event->duration - $timeElapsed;
            
            if ($timeRemaining < 2)
            {
                $this->endExam($exam);
                
                return retF('Exam was paused when time has already elapsed');
            }
            
            $exam[START_TIME] = date(self::TIME_FORMAT);
            
            $exam[END_TIME] = Carbon::parse($exam[START_TIME])->addSeconds($timeRemaining)->toDateTimeString();
            
            $exam['status'] = STATUS_ACTIVE;
        }
        elseif(empty($exam[START_TIME]))
        {
            $exam[START_TIME] = date(self::TIME_FORMAT);
            
            $exam[END_TIME] = Carbon::parse($exam[START_TIME])->addSeconds($event->duration)->toDateTimeString();
        }
        
        $arr['meta']['time_remaining'] = Carbon::now()->diffInSeconds(Carbon::parse($exam[END_TIME]), false);
        $arr['exam'] = $exam;
        
        $this->examHandler->syncExamFile($exam);
        
        return ['success' => true, MESSAGE => 'Exam started', 'data' => $arr, 'student' => $student];
    }
    
    function getExamAndEvent($examNo){
        
        $dirs = scandir(EventContentHandler::BASE_DIR);
        
        foreach ($dirs as $dir) 
        {
            if(!(strpos($dir, 'event') === 0)) continue;
//             if(!  starts_with($dir, 'event')) continue;
            
            // Getting here means the exam exisits
            $examFilename = EventContentHandler::BASE_DIR."$dir/exam-$examNo.json";
            $eventFilename = EventContentHandler::BASE_DIR."$dir/event.json";
            
            if(!file_exists($examFilename) || !file_exists($eventFilename)) continue;
            
            $exam = json_decode(file_get_contents($examFilename));
            $event = json_decode(file_get_contents($eventFilename));
            
            return ['exam' => $exam, 'event' => $event];
        }
        
        return null;
    }
    
    private function getSession($courseSessionId){
        
        $dirs = scandir(EventContentHandler::BASE_DIR);
        
        foreach ($dirs as $dir) 
        {
            if(!(strpos($dir, 'event') === 0)) continue;
//             if(!starts_with($dir, 'event')) continue;
            
            // Getting here means the exam exisits
            $sessionFilename = EventContentHandler::BASE_DIR."$dir/session-$courseSessionId.json";
            
            if(!file_exists($sessionFilename)) continue;
            
            $session = json_decode(file_get_contents($sessionFilename), true);
            
            return $session;
        }
        
        return null;
    }
    
    function endExam($exam)
    {
        if($exam['status'] == STATUS_ENDED) return retF('Exam already ended');
        
        $examSubjects = $exam['exam_subjects'];
        
        $totalScore = 0;
        $totalNumOfQuestions = 0;
        $arr = [];

        /** @var \App\Models\ExamSubjects $examSubject */
        foreach ($examSubjects as $examSubject)
        {
            $examSubject = (array) $examSubject;
            $session = $this->getSession($examSubject['course_session_id']);
            
            $scoreDetail = $this->examHandler->calculateScoreFromFile($exam, $examSubject['id'], $session['questions']);
            
            if(empty($scoreDetail['success'])) continue; //return $examSubject;// Score not calculated
            
            $score = $scoreDetail['score'];//$questionAttemptsModel->getScore($examSubject[TABLE_ID]);
            
            $numOfQuestions = $scoreDetail['num_of_questions'];//$questionsModel->getNumOfQuestions($examSubject[COURSE_CODE], $examSubject[COURSE_SESSION_ID]);
            
            $examSubject[SCORE] = $score;
            
            $examSubject[NUM_OF_QUESTIONS] = $numOfQuestions;
            
            $examSubject['status'] = STATUS_ENDED;
            
            $totalScore += $score;
            
            $totalNumOfQuestions += $numOfQuestions;
            $arr[] = $examSubject;
            //             dlog("totalScore = $totalScore");
        }
        
        $exam['exam_subjects'] = $arr;
        $exam['status'] = STATUS_ENDED;
        $exam[SCORE] = $totalScore;
        $exam[NUM_OF_QUESTIONS] = $totalNumOfQuestions;
        
        $this->examHandler->syncExamFile($exam);
        
        return retS('Exam ended');
    }
    
    function pauseExam($examNo)
    {
        $examAndEvent = $this->getExamAndEvent($examNo);
        
        $exam = (array)getValue($examAndEvent, 'exam');
        
        if (!$exam) return retF('Exam record not found');
        
        if($exam['status'] != STATUS_ACTIVE) return retF('Exam inactive');
        
        if(!$exam[END_TIME])
        {
            return retF('Exam has no end time');
        }
        elseif (Carbon::parse($exam[END_TIME])->getTimestamp() < Carbon::now()->getTimestamp())
        {
            $this->endExam($exam);
            
            return retF('Time elapsed');
        }
        
        $exam['status'] = STATUS_PAUSED;
        
        $exam[PAUSED_TIME] = date(self::TIME_FORMAT);
        
        $exam[END_TIME] = null;
        
        $this->examHandler->syncExamFile($exam);
        
        return retS('Exam paused');
    }
    
    function extendExam($exam, $mins) 
    {
        if($mins < 1) return retF("Set a valid extension time");
        
        if($exam['status'] !== STATUS_PAUSED && empty($exam[END_TIME])) {
            return retF("Exam has not started yet");
        }

        $currentEndTime = \Carbon\Carbon::parse($exam[END_TIME]);
        
        if($exam['status'] === STATUS_ENDED || \Carbon\Carbon::now()->diffInSeconds($currentEndTime, false) < 10)
        {
            $currentEndTime = \Carbon\Carbon::now();
            $exam['status'] = STATUS_ACTIVE;
        }
        
        if($exam['status'] === STATUS_ENDED && !empty($exam[PAUSED_TIME]))
        {
            $pauseTime = \Carbon\Carbon::parse($exam[PAUSED_TIME]);
            
            $exam[PAUSED_TIME] = $pauseTime->subMinute($mins);
        }
        else
        {
            $exam[END_TIME] = $currentEndTime->addMinute($mins);        
        }
        
        $this->examHandler->syncExamFile($exam);
        
        return retS("Exam time has been extende by {$mins}mins. Student should refresh his/her page.");
    }
    
}



