<?php
namespace App\Controllers\Admin;

use App\Models\BaseModel;
use App\Controllers\Admin\BaseAdmin;

class EventController extends BaseAdmin
{
	private $resultsDir = APP_DIR.'../public/files/';

	private $eventContentHelper;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Controllers\Helpers\EventContentHandler $eventContentHelper
    ){
        parent::__construct($c, $session);
        
        $this->eventContentHelper = $eventContentHelper;
	}

	function index() 
	{
	    $ret = $this->eventContentHelper->listEvents(APP_CENTER_CODE);
// 	    die(json_encode($ret));
	    if(!$ret['success']){
	        
	        $this->session->flash('error', $ret['message']);
	        
	        redirect_(getAddr('admin_dashboard'));
	    }
	    
		return $this->view('admin/events/all', [
			'allRecords' => $ret['result'],
		    'count' => $ret['count']
		]);
	}
    
	function downloadEventContent($event_id)
	{
	    $post = $_REQUEST;
	    $post['center_code'] = APP_CENTER_CODE;
	    $post['event_id'] = $event_id;
	    
	    $ret = $this->eventContentHelper->fetchEventContent($post);
        
	    $this->session->flash($ret['success']?'success':'error', $ret['message']);
        
        redirect_(getAddr('admin_event_index'));
	}
    
	function uploadEventResult($event_id)
	{
	    $ret = $this->eventContentHelper->uploadEventExams($event_id);
	    
	    $this->session->flash($ret['success']?'success':'error', $ret['message']);
	    
	    redirect_(getAddr('admin_event_index'));
	}


}





