<?php

namespace App\Models;

class Courses extends BaseModel {
	
	public $table = COURSES_TABLE;
	

	function insert($post) {
	    
	    $val = new \Valitron\Validator($post);
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Course: Input validation failed - '.formatValidationErrors($val->errors(), true), 'val_errors' => $val->errors()];
	    }
	    
// 	    if($this->where(COURSE_CODE, '=', $post[COURSE_CODE])
// 	        ->where(EXAM_CONTENT_ID, '=', $post[EXAM_CONTENT_ID])->first()){
// 	            return [SUCCESSFUL => false, MESSAGE => 'Error: Course code already exist'];
// 	    }
	    
// 	    $this[EXAM_CONTENT_ID] = $post[EXAM_CONTENT_ID];
	    $this[COURSE_CODE] = $post[COURSE_CODE];
	    $this[CATEGORY] = array_get($post, CATEGORY);
	    $this[COURSE_TITLE] = $post[COURSE_TITLE];
	    $this[DESCRIPTION] = array_get($post, DESCRIPTION);
	    
	    if ($this->save())
	    {
	        return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully'];
	    }
	    
	    return [SUCCESSFUL => false, MESSAGE => 'Data recorded successfully'];
	}
	
	function updateRecord($post) {
	    $val = new \Valitron\Validator($post);
	    
	    $val->rules($this->rules_update);
	    //Assign labels to the validator... Use to replace form name
	    $val->labels($this->labels);
	    if (!$val->validate())
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
	    }
	    
	    $old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
	    
	    //Check if row exists
	    if (!$old)
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'There is no existing record, create new one'];
	    }
	    
	    $arr = [];
	    
// 	    $arr[EXAM_CONTENT_ID] = $post[EXAM_CONTENT_ID];
	    $arr[COURSE_CODE] = $post[COURSE_CODE];
	    $arr[CATEGORY] = array_get($post, CATEGORY, $old[CATEGORY]);
	    $arr[COURSE_TITLE] = $post[COURSE_TITLE];
	    $arr[DESCRIPTION] = array_get($post, DESCRIPTION, $old[DESCRIPTION]);
	    
	    $success = $this->where(TABLE_ID, '=', $post[TABLE_ID])->update($arr);
	    
	    if ($success)
	    {
	        return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Error: Update failed'];
	}
	
	function examContent() {
	    return $this->belongsTo(\App\Models\ExamContent::class, EXAM_CONTENT_ID, TABLE_ID);
	}
	
	function sessions() {
	    return $this->hasMany(\App\Models\Sessions::class, COURSE_ID, TABLE_ID);
	}
	
	function summaryChapters() {
	    return $this->hasMany(\App\Models\Summary::class, COURSE_ID, TABLE_ID);
	}
	
}