<?php
namespace App\Models;


class Events extends BaseModel
{
    public $table = EVENTS_TABLE;
	
    public $fillable = [
        TABLE_ID, TITLE, DESCRIPTION, CENTER_CODE, DURATION, STATUS, NUM_OF_ACTIVATIONS
	];
	
	public $rules_insert = [
	    'required'   => [ [DURATION], [TITLE] ],
	    'digits'     => [ [CENTER_CODE] ],
	    'lengthMin'  => [ [CENTER_CODE, 3] ],
	    'lengthMax'  => [ [CENTER_CODE, 5] ],
	];
	
	public $rules_update = [
	    'required'   => [ [TABLE_ID], [DURATION], [TITLE] ],
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function validateParams($post, 
	    \App\Models\ExamCenters $examCenterModel
    ){
	    $val = $this->validatorWrapper->initValidator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
	    
	    if (!$examCenterModel->where(CENTER_CODE, '=', $post[CENTER_CODE])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Exam center not found'];
	    }
	    
        return [SUCCESS => true, MESSAGE => ''];
	}
	
	function insert($post, 
	    \App\Core\CodeGenerator $codeGenerator,
	    \App\Models\ExamCenters $examCenterModel
    ){	
	    $val = $this->validateParams($post, $examCenterModel);
	    
	    if(!$val[SUCCESSFUL]) return $val;
	    
	    if ($this->where(CENTER_CODE, '=', $post[CENTER_CODE])
	        ->where(TITLE, '=', $post[TITLE])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Title already Exists'];
	    }
	    
		$arr = [];
		$arr[TITLE] = htmlentities($post[TITLE]);
		$arr[DESCRIPTION] = htmlentities($post[DESCRIPTION]);
		$arr[CENTER_CODE] = htmlentities($post[CENTER_CODE]);
		$arr[DURATION] = htmlentities($post[DURATION]);
		$arr[STATUS] = STATUS_ACTIVE;

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $data];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post){
        $val = $this->validatorWrapper->initValidator($post);
        
        $val->rules($this->rules_update);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
        }
        
        $oldData = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
        
        if (!$oldData)
        {
            return [SUCCESS => false, MESSAGE => 'Parent record not found'];
        }
        
        $arr = [];
        $oldData[TITLE] = htmlentities(array_get($post, TITLE, $oldData[TITLE]));
        $oldData[DESCRIPTION] = htmlentities(array_get($post, DESCRIPTION, $oldData[DESCRIPTION]));
        $oldData[DURATION] = htmlentities(array_get($post, DURATION, $oldData[DURATION]));
        
        $success = $oldData->save();
        
        if ($success)
        {
            return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $oldData];
        }
        
        return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function centerEventsCount($centerCode)
	{
	    $sql = "SELECT COUNT(".TABLE_ID.") AS query_result FROM ".EVENTS_TABLE
	    . " WHERE ".CENTER_CODE."=:center_code";
	    
	    $superArray = $this->pdoQuery($sql, [':center_code' => $centerCode]);
	    
	    return array_get($superArray, 'query_result', 0);
	}
	
// 	function isSubscriptionActive(\App\Core\SubscriptionPlan $subscriptionPlan, $redirectAddr) 
// 	{
// 	    $isExpired = $subscriptionPlan->isExpired($redirectAddr);
	    
// 	    $plan = $subscriptionPlan->getPlan($redirectAddr);
	    
// 	    if($plan == \App\Core\SubscriptionPlan::SUBSCRIPTION_PLAN_PER_EVENT)
// 	    {
// 	        return [SUCCESSFUL => true, MESSAGE => '', 'plan' => $plan];
// 	    }
	    
// 	    if($isExpired)
// 	    {
// 	        return [SUCCESSFUL => false, MESSAGE => 'Subscription Expired'];
// 	    }
	    
//         return [SUCCESSFUL => true, MESSAGE => '', 'plan' => $plan];
// 	}
	
	function getActiveEvents($centerCode)
	{
	    return $this->where(STATUS, '=', STATUS_ACTIVE)->where(CENTER_CODE, '=', $centerCode)->get();
	}
	
	function eventSubjects()
	{
	    return $this->hasMany(\App\Models\EventSubjects::class, EVENT_ID, TABLE_ID);
	}
	
	function exams()
	{
	    return $this->hasMany(\App\Models\Exams::class, EVENT_ID, TABLE_ID);
	}
	
	function examCenter()
	{
	    return $this->belongsTo(\App\Models\ExamCenters::class, CENTER_CODE, CENTER_CODE);
	}
	
}


?>