<?php
namespace App\Models;


class Instructions extends BaseModel {

	public $table = INSTRUCTIONS_TABLE;
	
	public $fillable = [TABLE_ID, COURSE_ID, COURSE_SESSION_ID, INSTRUCTION, FROM_, TO_];
		
	function session() {
	    return $this->belongsTo(\App\Models\Sessions::class, COURSE_SESSION_ID, TABLE_ID);
	}

}