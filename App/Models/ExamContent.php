<?php

namespace App\Models;

class ExamContent extends BaseModel 
{
	public $table = EXAM_CONTENT_TABLE;
	
	public $rules_insert = [
	    'required' => [[EXAM_NAME]]
	];
	public $rules_update = [
	    'required' => [ [TABLE_ID], [EXAM_NAME] ]	    
	];
	
	function insert($post) 
	{
		$val = new \Valitron\Validator($post);  
		$val->rules($this->rules_insert);
		
		if (!$val->validate())
		{
		    return [SUCCESSFUL => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
		} 
		
		if($this->where(EXAM_NAME, '=', $post[EXAM_NAME])->first())
		{
		    return [SUCCESSFUL => false, MESSAGE => 'Error: Exam name already exist'];
		}
		
		$this[COUNTRY] = array_get($post, COUNTRY);
		$this[REGION] = array_get($post, REGION);
		$this[INSTITUTION] = array_get($post, INSTITUTION);
		$this[EXAM_NAME] = $post[EXAM_NAME];
		$this[FULLNAME] = array_get($post, FULLNAME);
		
		if ($this->save())
		{
		    return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully'];
		}
		
	    return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post) 
	{
		$val = new \Valitron\Validator($post);	
		$val->rules($this->rules_update);
		
		if (!$val->validate())
		{
		    return [SUCCESSFUL => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
		}
		
		$old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		//Check if row exists
		if (!$old)
		{
		    return [SUCCESSFUL => false, MESSAGE => 'Record not found'];
		}
		
		if($old[EXAM_NAME] != $post[EXAM_NAME] &&
		    $this->where(EXAM_NAME, '=', $post[EXAM_NAME])->first())
		{
		    return [SUCCESSFUL => false, MESSAGE => 'Error: Exam name already exist'];
		}
		
		$arr = [];
		$arr[EXAM_NAME] = $post[EXAM_NAME];
		$arr[COUNTRY] = array_get($post, COUNTRY);
		$arr[REGION] = array_get($post, REGION);
		$arr[INSTITUTION] = array_get($post, INSTITUTION);
		$arr[FULLNAME] = array_get($post, FULLNAME);		
		
		$success = $this->where(TABLE_ID, '=', $post[TABLE_ID])->update($arr);
	    
		if($success)
		{
		    return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
		}
		
	    return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
	}
	
	function courses() 
	{
		return $this->hasMany(\App\Models\Courses::class, EXAM_CONTENT_ID, TABLE_ID);
	}
	
	
}


