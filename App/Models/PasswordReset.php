<?php
namespace App\Models;

class PasswordReset extends BaseModel{

	public $table = PASSWORD_RESET_TABLE;
	
	public $fillable = [TABLE_ID, PHONE_NO, PASSWORD_RESET_CODE, NUM_OF_ATTEMPTS, EXPIRY_TIME, IS_STILL_VALID];

	
	function __construct($param = null)
	{
	    parent::__construct($param);
	    
// 	    if($param != INJECTED) return;
	    
	}
	
	function user() {
		return $this->belongsTo(\App\Models\Users::class, PHONE_NO, PHONE_NO);
	}

	
}
?>