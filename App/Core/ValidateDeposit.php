<?php
namespace App\Core;

class ValidateDeposit
{
    private $postData;
    private $transactionModel;
    
    function __construct(\App\Models\Transactions $transactionModel) 
    {
        $this->transactionModel = $transactionModel;
    }
    
    private function validatePinDeposit() 
    {
//         if($this->postData[NETWORK] != NETWORK_MTN)
        return $this->validatPin();
    }
    
    private function airtimeTransfer()
    {
        return $this->validatPhoneNumber();
    }
    
    function validate($postData)
    {
        $this->postData = $postData;
        
        $network = array_get($this->postData, NETWORK);
        
        $amount = (int)array_get($this->postData, AMOUNT);
        
        if(!$network || !$amount) return [SUCCESSFUL => false, MESSAGE => 'Fill all fields'];
        
        if(!in_array($network, \Config::NETWORKS)) return [SUCCESSFUL => false, MESSAGE => 'Unrecognized network'];
        
        if($amount < 50) return [SUCCESSFUL => false, MESSAGE => 'Amount cannot be less than ' . NAIRA_SIGN . '50'];
        
        if($amount > 100000) return [SUCCESSFUL => false, MESSAGE => 'Amount cannot be greater than ' . NAIRA_SIGN . number_format(100000)];
        
        $pinDepositNetworks = [NETWORK_AIRTEL, NETWORK_9_MOBILE, NETWORK_MTN, NETWORK_GLO];
        
        if(in_array($network, $pinDepositNetworks)) return $this->validatePinDeposit();
        
        return $this->airtimeTransfer();
    }
    
    private function validatPin() 
    {
        $pin = array_get($this->postData, PIN);
        
        if(empty($pin)) return [SUCCESSFUL => false, MESSAGE => 'Pin is required'];
        
        if(!ctype_digit($pin)) return [SUCCESSFUL => false, MESSAGE => 'Airtime Pin must be in digits'];
        
        $len = strlen($pin);
        
        if ($len != 15 && $len != 16 && $len != 12 && $len != 17) return [SUCCESSFUL => false, MESSAGE => 'Airtime PIN must be 12, 15, 16 or 17 digits'];
        
        return [SUCCESSFUL => true, MESSAGE => 'Pin Validated'];
    }
    
    private function validatPhoneNumber() 
    {
        $phone = array_get($this->postData, PHONE_NO); 
        
        if(empty($phone)) return [SUCCESSFUL => false, MESSAGE => 'Phone number is required'];
        
        if(!ctype_digit($phone)) return [SUCCESSFUL => false, MESSAGE => 'Phone number must be in digits'];
        
        $len = strlen($phone);
        
        if ($len != 11) return [SUCCESSFUL => false, MESSAGE => 'Phone number must be a total of 11 digits'];
        
        return [SUCCESSFUL => true, MESSAGE => 'Phone number Validated'];
    }
    
    
    
}







