<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Events{

	public $connection = 'default';

	function __construct() {
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(EVENTS_TABLE))
		{
		    $schema->table(EVENTS_TABLE, function(Blueprint $table) use ($schema) {
		    
		        if(!$schema->hasColumn(EVENTS_TABLE, NUM_OF_ACTIVATIONS))
		        {
    		        $table->integer(NUM_OF_ACTIVATIONS, false, true)->default(0);
    		        
    		        echo 'Events Table updated <br />';
		        }
		    });
		    
			echo 'Events already exists';
			
			return;
		}

		$schema->create(EVENTS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->string(TITLE); 
		    $table->string(DESCRIPTION)->nullable(true); 
		    $table->string(CENTER_CODE, 4); 
		    $table->string(DURATION, 10);
		    $table->string(STATUS)->default(STATUS_ACTIVE);
		    $table->integer(NUM_OF_ACTIVATIONS, false, true)->default(0);
		    
		    // 		    $table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
            $table->foreign(CENTER_CODE)->references(CENTER_CODE)->on(EXAM_CENTERS_TABLE)
		          ->onUpdate('cascade')->onDelete('cascade');
		    
			echo 'Events table created';
		});


	}

}