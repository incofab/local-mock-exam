<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Exams{

	public $connection = 'default';

	function __construct() {
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(EXAMS_TABLE))
		{
		    $schema->table(EXAMS_TABLE, function(Blueprint $table) use ($schema) {
		        if($schema->hasColumn(EXAMS_TABLE, SCORE)) return;
		        
		        $table->integer(SCORE, false, true)->nullable(true);
		        
		        $table->integer(NUM_OF_QUESTIONS, false, true)->nullable(true);
		        
		        echo 'Exam Subjects Table updated <br />';
		    });
			echo 'Exams already exists';
			
			return;
		}

		$schema->create(EXAMS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->integer(EVENT_ID, false, true);
		    $table->string(EXAM_NO, 15)->unique();
		    $table->string(STUDENT_ID, 15); 
// 		    $table->string(DURATION, 10);
		    $table->dateTime(START_TIME)->nullable(true);
		    $table->dateTime(PAUSED_TIME)->nullable(true);
		    $table->dateTime(END_TIME)->nullable(true);
		    $table->integer(SCORE, false, true)->nullable(true);
		    $table->integer(NUM_OF_QUESTIONS, false, true)->nullable(true);
		    $table->string(STATUS)->default(STATUS_ACTIVE);
		    
		    // 		    $table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
		    $table->foreign(STUDENT_ID)->references(STUDENT_ID)->on(STUDENTS_TABLE)
		    ->onDelete('cascade')->onUpdate('cascade');
		    
            $table->foreign(EVENT_ID)->references(TABLE_ID)->on(EVENTS_TABLE)
            ->onDelete('cascade')->onUpdate('cascade');
		    
			echo 'Exams table created';
		});


	}

}