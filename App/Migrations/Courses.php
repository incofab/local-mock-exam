<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Courses{
	
	public $connection = 'default'; 
	
	function __construct() {  
		$this->create_courses_Table();   
	} 
	
	function create_courses_Table() {

		$schema = Capsule::schema($this->connection); 
		
		if ($schema->hasTable(COURSES_TABLE)){ 
			echo 'Courses Table already exists';
			return;
		}
		
		$schema->create(COURSES_TABLE, function(Blueprint $table) {
		    
		    $table->increments(TABLE_ID);
		    $table->string(COURSE_CODE, 80);
		    $table->string(CATEGORY, 20)->nullable(true);
		    $table->string(COURSE_TITLE)->nullable(true);
		    $table->text(DESCRIPTION)->nullable(true);
		    
// 		    $table->boolean(IS_FILE_CONTENT_UPLOADED)->default(false);
// 		    $table->integer(EXAM_CONTENT_ID, false, true)->nullable(true);
		    
		    // 			$table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    
// 		    $table->foreign(EXAM_CONTENT_ID)->references(TABLE_ID)->on(EXAM_CONTENT_TABLE)
// 		    ->onDelete('cascade')->onUpdate('cascade');
		    
		    $table->engine = 'InnoDB';
		    
		    echo 'Courses table created';
		});
		
				
	}
	
}