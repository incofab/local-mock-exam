<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Users{

	public $connection = 'default';
  
	function __construct() {
		$this->create_Users_Table();
	}

	function create_Users_Table() {

		$schema = Capsule::schema();
 
		if ($schema->hasTable(USERS_TABLE))
		{
			echo 'Users Table already exists';
			return;
		}

		$schema->create(USERS_TABLE, function(Blueprint $table) {
				
			$table->increments(TABLE_ID);
			$table->string(USERNAME, 40)->unique();
			$table->string(PASSWORD, 72);
			$table->string(FULLNAME, 150)->nullable(true);
			$table->string(FIRSTNAME, 70)->nullable(true);
			$table->string(LASTNAME, 70)->nullable(true);
			$table->string(OTHERNAMES, 100)->nullable(true);
			$table->float(BALANCE)->default(0);
			$table->float(BONUS_BALANCE)->default(0);
			$table->integer(POINTS, false, true)->default(0);
			$table->string(ACCOUNT_NUMBER, 20)->nullable(true);
			$table->string(BANK_NAME, 50)->nullable(true);
			$table->string(EMAIL, 50)->nullable(true);
			$table->text(PROFILE_PIC)->nullable(true);
			$table->string(PHONE_NO, 15)->unique();
			$table->string(ADDRESS)->nullable(true);
			$table->string(COUNTRY, 40)->nullable(true);
			$table->boolean(ACTIVATED)->default(FALSE);
			$table->boolean(SUSPENDED)->default(FALSE);
			$table->boolean(IS_VIRTUAL_USER)->default(FALSE);
			$table->string(GENDER, 15)->nullable(true);
			$table->date(DOB)->nullable(true);
			$table->string(FB_ACCESS_TOKEN)->nullable(true);
			$table->string(FB_ID)->nullable(true);
			$table->integer(AGE, false, true)->default(20);
			$table->string(REMEMBER_LOGIN)->nullable(true);
			$table->integer(FAILED_LOGIN_COUNT, false, true)->default(0);
			$table->integer(FIRST_FAILED_LOGIN, false, true)->default(0);
			
				
			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';
 
			echo 'Users table created';
			
		});


	}

}