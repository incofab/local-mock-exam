<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class EventSubjects{

	public $connection = 'default';

	function __construct() {
		$this->createTable();
	}

	function createTable() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(EVENT_SUBJECTS_TABLE))
		{
			echo 'Event Subjects already exists';
			
			return;
		}

		$schema->create(EVENT_SUBJECTS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->integer(EVENT_ID, false, true);
		    $table->integer(COURSE_ID, false, true); 
		    $table->integer(COURSE_SESSION_ID, false, true);
		    $table->string(STATUS)->default(STATUS_ACTIVE);
		    
		    // 		    $table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
		    $table->foreign(EVENT_ID)->references(TABLE_ID)->on(EVENTS_TABLE)
		          ->onDelete('cascade')->onUpdate('cascade');
		    
	        $table->foreign(COURSE_ID)->references(TABLE_ID)->on(COURSES_TABLE)
	               ->onDelete('cascade')->onUpdate('cascade');
		    
            $table->foreign(COURSE_SESSION_ID)->references(TABLE_ID)->on(SESSIONS_TABLE)
	               ->onDelete('cascade')->onUpdate('cascade');
		    
			echo 'Event Subjects table created';
		});


	}

}