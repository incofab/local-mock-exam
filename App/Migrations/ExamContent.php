<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class ExamContent{
	
	public $connection = 'default'; 
	
	function __construct() {  
		$this->create_courses_Table();   
	} 
	
	function create_courses_Table() {

		$schema = Capsule::schema($this->connection); 
		
		if ($schema->hasTable(EXAM_CONTENT_TABLE)){ 
		    
		    $schema->table(EXAM_CONTENT_TABLE, function(Blueprint $table) use ($schema)
		    {
		        if(!$schema->hasColumn(EXAM_CONTENT_TABLE, DESCRIPTION))
		        {
        			$table->text(DESCRIPTION)->nullable(true);
		            
		            echo DESCRIPTION.' column added <br />';
		        }
		    });
		    
		    echo EXAM_CONTENT_TABLE.' Table already exists';
		    
			return;
		}
		
		$schema->create(EXAM_CONTENT_TABLE, function(Blueprint $table) {
			
			$table->increments(TABLE_ID);
			$table->string(COUNTRY)->nullable(true);
			$table->string(REGION)->nullable(true);
			$table->string(INSTITUTION)->nullable(true);
			$table->string(EXAM_NAME)->unique();
			$table->text(FULLNAME)->nullable(true);
			$table->boolean(IS_FILE_CONTENT_UPLOADED)->default(false);
			$table->text(DESCRIPTION)->nullable(true);

			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			
			$table->engine = 'InnoDB';
			
			echo EXAM_CONTENT_TABLE.' table created';
		});
	}
	
	
}