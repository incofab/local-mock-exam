<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Settings
{
	public $connection = 'default';

	function __construct() 
	{
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(SETTINGS_TABLE))
		{
// 		    $schema->table(SETTINGS_TABLE, function(Blueprint $table) use ($schema) {
// 		        if(!$schema->hasColumn(SETTINGS_TABLE, NUM_OF_ACTIVATIONS))
// 		        {
//     		        $table->integer(NUM_OF_ACTIVATIONS, false, true)->default(0);
//     		        echo 'Events Table updated <br />';
// 		        }
// 		    });
			echo 'Settings already exists';
			
			return;
		}

		$schema->create(SETTINGS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->string(KEY); 
		    $table->text(VALUE)->nullable(true); 
		    $table->text(DESCRIPTION)->nullable(true); 
		    
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
			echo 'Settings table created';
		});

	}

}