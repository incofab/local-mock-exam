<?php

define('APP_DIR', __DIR__ . '/../App/');

require APP_DIR . '../config/config.php';
require APP_DIR . '../config/k.php';

if(empty($_REQUEST['exam_no'])){
    
    require APP_DIR . '../resources/views/html/exam_login.php';
    
    return;
}

require APP_DIR . 'Controllers/Helpers/ExamHandler.php';
require APP_DIR . 'Controllers/Helpers/ExamHelper.php';
require APP_DIR . 'Controllers/Helpers/EventContentHandler.php';

$examHandler = new \App\Controllers\Helpers\ExamHandler();
$examHelper = new \App\Controllers\Helpers\ExamHelper($examHandler);

$examNo = $_REQUEST['exam_no'];

$ret = $examHelper->startExam($examNo);

if(!$ret['success'] || empty($ret['data']))
{
    $error = $ret['message'];
    
    require APP_DIR . '../resources/views/html/exam_login.php';
    
    return;
}

$student = (array)$ret['student'];
$student['exam_no'] = $examNo;
$student['token'] = $student['id'];
$pageData = [
    'exam_data' => $ret['data'],
    'data' => $student
];

$examData = $pageData;
$eventId = $ret['data']['exam']['event_id'];

require APP_DIR . '../resources/views/html/react-exam-page.php';

return;
