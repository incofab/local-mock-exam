<?php

define('APP_DIR', __DIR__ . '/../App/');

require APP_DIR . '../config/config.php';
require APP_DIR . '../config/k.php';

require APP_DIR . 'Controllers/Helpers/ExamHandler.php';
require APP_DIR . 'Controllers/Helpers/ExamHelper.php';

$examHandler = new \App\Controllers\Helpers\ExamHandler();
$examHelper = new \App\Controllers\Helpers\ExamHelper($examHandler);


$examNo = $_REQUEST['exam_no'];

$ret = $examHelper->pauseExam($examNo);

die(json_encode($ret));
