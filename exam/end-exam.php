<?php








/**********************************
 * @deprecated
 * Not needed at all
 **********************************/












define('APP_DIR', __DIR__ . '/../App/');

require APP_DIR . '../config/config.php';
require APP_DIR . '../config/k.php';

require APP_DIR . 'Controllers/Helpers/ExamHandler.php';
require APP_DIR . 'Controllers/Helpers/ExamHelper.php';

$examHandler = new \App\Controllers\Helpers\ExamHandler();
$examHelper = new \App\Controllers\Helpers\ExamHelper($examHandler);


$examNo = $_REQUEST['exam_no'];

$examAndEvent = $examHelper->getExamAndEvent($examNo);

$exam = (array) isset($examAndEvent['exam']) ? $examAndEvent['exam'] : [];

if (!$exam) return retF('Exam record not found');

$ret = $examHelper->endExam($exam);

die(json_encode($ret));
