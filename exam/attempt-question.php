<?php

/**
 * The directory to the app files
 * @var 
 */
define('APP_DIR', __DIR__ . '/../App/');

// require APP_DIR . '../config/config.php';
require APP_DIR . 'Controllers/Helpers/ExamHandler.php';

$examHandler = new \App\Controllers\Helpers\ExamHandler();

    $input = @file_get_contents("php://input");
    $post = json_decode($input, true);

//     dlog_22($post);
    $allAttempts = $post['attempts'];
    
//     dlog_22($allAttempts);
    
    $ret = $examHandler->attemptQuestion($allAttempts, 
        $post['event_id'], $post['exam_no'], $post['student_id']);
    
//     dlog_22($ret);
    
    if($ret['success'] !== true) emitResponse($ret);
    
    emitResponse([
        'success' => true, 
        'data' => ['success' => array_values($allAttempts), 'failure' => []]
    ]);
    
// }
// else 
// {
//     emitResponse(['success' => false, 'message' => 'Unknown type']);
// }

function emitResponse($data) 
{
    die(json_encode($data));
}


function dlog_22($msg) {
    $str = '';
    
    if (is_array($msg)) $str = json_encode($msg, JSON_PRETTY_PRINT);
    
    else $str = $msg;
    
    error_log(
        '*************************************' . PHP_EOL .
        '     Date Time: ' . date('Y-m-d h:m:s') . PHP_EOL .
        '------------------------------------' . PHP_EOL .
        $str . PHP_EOL . PHP_EOL .
        '*************************************' . PHP_EOL,
        
        3, APP_DIR . '../public/errorlog.txt');
    
}