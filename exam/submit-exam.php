<?php

define('APP_DIR', __DIR__ . '/../App/');

require APP_DIR . '../config/config.php';
require APP_DIR . '../config/k.php';

require APP_DIR . 'Controllers/Helpers/ExamHandler.php';
require APP_DIR . 'Controllers/Helpers/ExamHelper.php';
require APP_DIR . 'Controllers/Helpers/EventContentHandler.php';

$examHandler = new \App\Controllers\Helpers\ExamHandler();
$examHelper = new \App\Controllers\Helpers\ExamHelper($examHandler);


$input = @file_get_contents("php://input");
// dlog($input);
    
$post = json_decode($input, true);

// dlog($post);

$examNo = $post['exam_no'];

/*** Handle pending question attempts ****/

$allAttempts = isset($post['attempts']) ? $post['attempts'] : [];

$ret = $examHandler->attemptQuestion($allAttempts,
    $post['event_id'], $post['exam_no'], $post['student_id']);

// dlog($ret);
/*** // Handle pending question attempts****/


/*** End exam ****/

$examAndEvent = $examHelper->getExamAndEvent($examNo);

$exam = (array) (isset($examAndEvent['exam']) ? $examAndEvent['exam'] : []);

if (!$exam){
    die(json_encode(retF('Exam record not found')));
}

$ret = $examHelper->endExam($exam);

die(json_encode($ret));
